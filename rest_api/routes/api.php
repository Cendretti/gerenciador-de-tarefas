<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//C.R.U.D. tarefas
Route::prefix('tarefas')->group(
    function () {
        Route::post('/','TarefaController@save');
        Route::get('/','TarefaController@getList');
        Route::get('/{id}','TarefaController@getData');
        Route::put('/{id}','TarefaController@update');
        Route::delete('/{id}','TarefaController@delete');
    }
);
