<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tarefas extends Model
{
    protected $fillabe = [
        'nome', 'descricao', 'prazo', 'prioridade', 'concluida', 'is_active'
    ];
}
