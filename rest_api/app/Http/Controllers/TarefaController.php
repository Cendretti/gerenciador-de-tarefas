<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Tarefas;

class TarefaController extends Controller
{
    /**
     * Salva todas as tarefas
     * @param Request $request
     * 
     * @return JsonResponse
     */
    public function save(Request $request): JsonResponse
    {
        try {

            function transformaData($data)
            {
                if ($data == "") {
                    return $data = null;
                } else {
                    $dados = explode('/', $data);
                    $data_mysql = "{$dados[2]}-{$dados[1]}-{$dados[0]}";

                    return $data_mysql;
                }
            }

            // $validateData = $request->validate([
            //     'nome' => 'required|max:40',
            //     'propriedade' => 'required',
            //     'concluida' => 'required'
            // ]);

            $nome = $request->input('nome');
            $descricao = $request->input('descricao');
            $prazo = $request->input('prazo');
            $prioridade = $request->input('prioridade');
            $concluida = $request->input('concluida');

            $tarefa = new Tarefas();
            $tarefa->nome = $nome;
            $tarefa->descricao = $descricao;
            $tarefa->prazo = transformaData($prazo);
            $tarefa->prioridade = $prioridade;
            $tarefa->concluida = $concluida;

            $success = $tarefa->save();

            return response()->json(['success' => $success, 'data: ' => $prazo], 200);
        } catch (\Exception $ex) {
            $response = [
                'message' => "Erro ao registrar tarefas",
                'exception' => $ex,
            ];

            return response()->json($response, 500);
        }
    }

    /**
     * Retorna uma lista de tarefas
     * 
     * @return JsonResponse
     */
    public function getList(): JsonResponse
    {
        try {
            $tarefa = new Tarefas();

            $success = $tarefa->get();

            return response()->json($success, 200);
        } catch (\Exception $ex) {
            $response = [
                'message' => "Erro ao listar as tarefas",
                'exception' => $ex,
            ];
            return response()->json($response, 500);
        }
    }

    /**
     * Retorna uma unica Tarefa
     * @param $id
     * 
     * @return JsonResponse
     */
    public function getData(int $id): JsonResponse
    {
        try {
            $tarefa = Tarefas::where('id', $id)->get();

            if (!count($tarefa)) {
                return response()->json('', 204);
            }

            return response()->json($tarefa, 200);
        } catch (\Exception $ex) {
            $response = [
                'message' => "Erro ao obter essa tarefa",
                'exception' => $ex,
            ];
            return response()->json($response, 500);
        }
    }

    /**
     * Atualiza uma tarefa
     * 
     * @param int $id
     * @param Request $request
     * 
     * @return JsonResponse
     */
    public function update(int $id, Request $request): JsonResponse
    {
        try {
            function transformaDataa($data)
            {
                if ($data == "") {
                    return $data = null;
                } else {
                    $dados = explode('/', $data);
                    $data_mysql = "{$dados[2]}-{$dados[1]}-{$dados[0]}";

                    return $data_mysql;
                }
            }
            $tarefa = Tarefas::findOrFail($id);

            $tarefa->nome = $request->input('nome');
            $tarefa->descricao = $request->input('descricao');
            $tarefa->prazo = transformaDataa($request->input('prazo'));
            $tarefa->prioridade = $request->input('prioridade');
            $tarefa->concluida = $request->input('concluida');

            $success = $tarefa->save();

            return response()->json(['succsess' => $success], 200);
        } catch (\Exception $ex) {
            $response = [
                'message' => "Erro ao atualizar tarefa",
                'exception' => $ex,
            ];
            return response()->json($response, 500);
        }
    }

    /**
     * Deleta uma Tarefa
     * 
     * @param int $id
     * 
     * @return JsonResponse
     */
    public function delete(int $id): JsonResponse
    {
        try {
            $tarefa = Tarefas::findOrFail($id);

            $success = $tarefa->delete();

            return response()->json(['succsess' => $success], 200);
        } catch (\Exception $ex) {
            $response = [
                'message' => "Erro ao deletar a tarefa tarefa",
                'exception' => $ex,
            ];
            return response()->json($response, 500);
        }
    }
}
